import com.mybatis.test.dao.EmpMapper;
import com.mybatis.test.pojo.DepartMent;
import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class TestCache {

    public SqlSessionFactory getSqlSessionFactory() throws IOException{
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        return sqlSessionFactory;
    }


   @org.junit.Test
    public void test(){
       String resource = "mybatis-config.xml";
       InputStream inputStream = null;
       try {
           inputStream = Resources.getResourceAsStream(resource);
       } catch (IOException e) {
           e.printStackTrace();
       }
       SqlSessionFactory sqlSessionFactory =
               new SqlSessionFactoryBuilder().build(inputStream);
       SqlSession sqlSession = sqlSessionFactory.openSession();
       EmpMapper mapper = sqlSession.getMapper(EmpMapper.class);
       Emp emp = mapper.selectEmpById(1);
       System.out.println(emp);
       Emp emp1 = mapper.selectEmpById(1);
       System.out.println(emp1);
       System.out.println(emp==emp1);
       sqlSession.close();
   }

    @org.junit.Test
    public void test01(){
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //sqlsession不同
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmpMapper mapper = sqlSession.getMapper(EmpMapper.class);
        Emp emp = mapper.selectEmpById(1);
        System.out.println(emp);
        SqlSession sqlSession1 = sqlSessionFactory.openSession(true);
        EmpMapper mapper1 = sqlSession1.getMapper(EmpMapper.class);
        Emp emp1 = mapper1.selectEmpById(1);
        System.out.println(emp1);
        System.out.println(emp==emp1);
        sqlSession.close();
    }

    @org.junit.Test
    public void test02(){
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmpMapper mapper = sqlSession.getMapper(EmpMapper.class);
        //sqlsession相同 查询条件不同
        /*Emp emp = mapper.selectEmpById(1);
        System.out.println(emp);
        Emp emp1 = mapper.selectEmpById(3);
        System.out.println(emp1);
        System.out.println(emp==emp1);*/
        //sqlsession相同 执行了增删改操作
        /*Emp emp = mapper.selectEmpById(1);
        System.out.println(emp);
        mapper.addEmp(new Emp("haha1","0","110dbew",new DepartMent(2,"技术部")));
        Emp emp1 = mapper.selectEmpById(1);
        System.out.println(emp1);
        System.out.println(emp==emp1);
        sqlSession.close();*/
        //sqlsession相同，手动清除缓存
        Emp emp = mapper.selectEmpById(1);
        System.out.println(emp);
        sqlSession.clearCache();
        Emp emp1 = mapper.selectEmpById(1);
        System.out.println(emp1);
        System.out.println(emp==emp1);
        sqlSession.close();
    }

    /*
    * 二级缓存
    * */
    @org.junit.Test
    public void test03(){
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //sqlsession不同
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmpMapper mapper = sqlSession.getMapper(EmpMapper.class);
        Emp emp = mapper.selectEmpById(1);
        System.out.println(emp);
        sqlSession.close();//会话关闭，数据转交到二级缓存
        SqlSession sqlSession1 = sqlSessionFactory.openSession(true);
        EmpMapper mapper1 = sqlSession1.getMapper(EmpMapper.class);
        Emp emp1 = mapper1.selectEmpById(1);
        System.out.println(emp1);
        System.out.println(emp==emp1);
        sqlSession1.close();
    }







}
