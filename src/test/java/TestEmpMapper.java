import com.mybatis.test.dao.EmpMapper;
import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class TestEmpMapper {

    public SqlSessionFactory getSqlSessionFactory() throws IOException{
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        return sqlSessionFactory;
    }

    /**
     * 使用最初的方法
     * */
   @org.junit.Test
    public void test(){
       String resource = "mybatis-config.xml";
       InputStream inputStream = null;
       try {
           inputStream = Resources.getResourceAsStream(resource);
       } catch (IOException e) {
           e.printStackTrace();
       }
       SqlSessionFactory sqlSessionFactory =
               new SqlSessionFactoryBuilder().build(inputStream);
       SqlSession sqlSession = sqlSessionFactory.openSession();
       //参数1：命名空间+id
       //参数2：参数
       Emp emp = sqlSession.selectOne("com.mybatis.test.dao.selectEmp", 1);
       System.out.println(emp.toString());
       sqlSession.close();
   }

   /**
    *将接口与mapper.xml进行绑定  推荐使用
    * */
   @org.junit.Test
   public void test01(){
       SqlSessionFactory sqlSessionFactory = null;
       try {
           sqlSessionFactory = getSqlSessionFactory();
       } catch (IOException e) {
           e.printStackTrace();
       }
       SqlSession sqlSession = sqlSessionFactory.openSession();
       EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
       Emp emp = empMapper.selectEmpById(1);
       System.out.println(emp);
       sqlSession.close();
   }

   @org.junit.Test
   public void test03(){
       SqlSessionFactory sqlSessionFactory = null;
       try {
           sqlSessionFactory = getSqlSessionFactory();
       } catch (IOException e) {
           e.printStackTrace();
       }
       SqlSession sqlSession = sqlSessionFactory.openSession();
       EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
       Emp emp=new Emp(1,"lina");
       emp= empMapper.selectByIdAndName(1,"lina");
       System.out.println(emp);
       sqlSession.close();
   }

    @org.junit.Test
    public void test04(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
        Emp emp=new Emp(1,"lina");
        emp= empMapper.selectByEmp(emp);
        System.out.println(emp);
        sqlSession.close();
    }

    @org.junit.Test
    public void test05(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession();
        EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
        Emp emp = empMapper.selectByIdAndTable("emp", 1);
        System.out.println(emp);
        sqlSession.close();
    }

    @org.junit.Test
    public void test06(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
        Emp emp=new Emp();
        emp.setLastName("liaozan");
        emp.setGender("1");
        emp.setEmail(null);

        System.out.println(empMapper.addEmp(emp));
        sqlSession.close();
    }

    @org.junit.Test
    public void test07(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
        List<Emp> listByNameLike = empMapper.getListByNameLike("%i%");  //推荐使用这种  可以自由确定规则
        for(Emp e:listByNameLike){
            System.out.println(e.toString());
        }
        sqlSession.close();
    }

    @org.junit.Test
    public void test08(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
        Map<String, Object> lina = empMapper.getMapByName("lina");
        Map<Integer, Emp> mapByNameLike = empMapper.getMapByNameLike("%i%");
        System.out.println(lina);
        System.out.println(mapByNameLike);
        sqlSession.close();
    }




}
