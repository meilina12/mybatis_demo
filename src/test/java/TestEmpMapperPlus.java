import com.mybatis.test.dao.DepartMentMapper;
import com.mybatis.test.dao.EmpMapperPlus;
import com.mybatis.test.pojo.DepartMent;
import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class TestEmpMapperPlus {

    public SqlSessionFactory getSqlSessionFactory() throws IOException{
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        return sqlSessionFactory;
    }


   @org.junit.Test
    public void test(){
       SqlSessionFactory sqlSessionFactory = null;
       try {
           sqlSessionFactory = getSqlSessionFactory();
       } catch (IOException e) {
           e.printStackTrace();
       }
       SqlSession sqlSession = sqlSessionFactory.openSession(true);
       EmpMapperPlus mapper = sqlSession.getMapper(EmpMapperPlus.class);
       Emp emp = mapper.getById(1);
       System.out.println(emp);
       sqlSession.close();
   }
    @org.junit.Test
    public void test01(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapperPlus mapper = sqlSession.getMapper(EmpMapperPlus.class);
        Emp emp = mapper.getEmployeeAndDept(1);
        System.out.println(emp);
        sqlSession.close();
    }

    @org.junit.Test
    public void test02(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapperPlus mapper = sqlSession.getMapper(EmpMapperPlus.class);
        Emp emp = mapper.getEmployeeAndDept2(1);
        System.out.println(emp);
        sqlSession.close();
    }

    @org.junit.Test
    public void test03(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapperPlus mapper = sqlSession.getMapper(EmpMapperPlus.class);
        Emp emp = mapper.getEmployeeAndDeptStep(1);
        System.out.println(emp);
        sqlSession.close();
    }

    @org.junit.Test
    public void test04(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmpMapperPlus mapper = sqlSession.getMapper(EmpMapperPlus.class);
        Emp emp = mapper.getEmployeeAndDeptStep(1);
        System.out.println(emp.getLastName());
        System.out.println(emp.getDepartMent());
        sqlSession.close();
    }

    @org.junit.Test
    public void test05(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        DepartMentMapper mapper = sqlSession.getMapper(DepartMentMapper.class);
        DepartMent dept = mapper.getByIdAndMultEmp(1);
        System.out.println(dept);

        sqlSession.close();
    }

    @org.junit.Test
    public void test06(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        DepartMentMapper mapper = sqlSession.getMapper(DepartMentMapper.class);
        System.out.println(mapper.getByIdDept(1).getName());
        System.out.println(mapper.getByIdDept(1).getEmps());


        sqlSession.close();
    }






}
