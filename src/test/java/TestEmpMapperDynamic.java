import com.mybatis.test.dao.DepartMentMapper;
import com.mybatis.test.dao.EmpMapperPlus;
import com.mybatis.test.dao.EmployeeDynamicSQL;
import com.mybatis.test.pojo.DepartMent;
import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestEmpMapperDynamic {

    public SqlSessionFactory getSqlSessionFactory() throws IOException{
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory =
                new SqlSessionFactoryBuilder().build(inputStream);
        return sqlSessionFactory;
    }


   @org.junit.Test
    public void test(){
       SqlSessionFactory sqlSessionFactory = null;
       try {
           sqlSessionFactory = getSqlSessionFactory();
       } catch (IOException e) {
           e.printStackTrace();
       }
       SqlSession sqlSession = sqlSessionFactory.openSession(true);
       EmployeeDynamicSQL mapper = sqlSession.getMapper(EmployeeDynamicSQL.class);
       Emp emp=new Emp();
       DepartMent dept = new DepartMent();
       emp.setDepartMent(dept);
       emp.setId(1);
       emp.setLastName("%i%");
//       emp.setGender("0");
//       emp.setDepartMent();
       List<Emp> emp1 = mapper.getEmpByConditionIf(emp);
       List<Emp> emp11 = mapper.getEmpByConditionIf1(emp);
       List<Emp> empByConditionTrim = mapper.getEmpByConditionTrim(emp);
       List<Emp> empByConditionChoose = mapper.getEmpByConditionChoose(emp);
       System.out.println(empByConditionChoose);
       sqlSession.close();
   }

    @org.junit.Test
    public void test01(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDynamicSQL mapper = sqlSession.getMapper(EmployeeDynamicSQL.class);
        Emp emp=new Emp();
        DepartMent dept = new DepartMent();
        emp.setDepartMent(dept);
        emp.setId(1);
        emp.setLastName("丽娜");
        emp.setEmail("1105503741@qq.com");
//       emp.setGender("0");
//       emp.setDepartMent();
//        mapper.updateEmp(emp);
//        mapper.updateEmpSet(emp);
        mapper.updateEmpTrim(emp);
        sqlSession.close();
    }

    @org.junit.Test
    public void test02(){
        SqlSessionFactory sqlSessionFactory = null;
        try {
            sqlSessionFactory = getSqlSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        EmployeeDynamicSQL mapper = sqlSession.getMapper(EmployeeDynamicSQL.class);
        List<Emp> empByidsForeach = mapper.getEmpByidsForeach(Arrays.asList(1, 2, 3, 4, 6));
        System.out.println(empByidsForeach);
        List<Emp> empByidsForeach1 = mapper.getEmpByidsForeach1(Arrays.asList(1, 2, 3, 4, 6));
        System.out.println(empByidsForeach1);
        List<Emp> emps=new ArrayList<Emp>();
        emps.add(new Emp("lina1","0","aajxsjh",new DepartMent(1,"技术部")));
        emps.add(new Emp("lina2","0","aajxsjh",new DepartMent(1,"技术部")));
        emps.add(new Emp("lina3","0","aajxsjh",new DepartMent(1,"技术部")));
        System.out.println(mapper.addEmpsForeach1(emps));
        sqlSession.close();
    }


}
