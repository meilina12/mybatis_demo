package com.mybatis.test.dao;

import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EmpMapper {

    public Emp selectEmpById(int id);
    public Emp selectEmpByDeptId(int deptId);

    public Emp selectByIdAndName(int id,String lastName);

    public Emp selectByEmp(Emp emp);

    Emp selectByIdAndTable(String table,int id);

    public int addEmp(Emp emp);

    List<Emp> getListByNameLike(@Param("name") String name);

    //得到一条记录  将这条记录封装成map  key是字段名 value是字段值
    Map<String,Object> getMapByName(String name);
    //得到多条记录  将这些记录封装成map  key：javabean的任意一个属性（id）  value是这一条记录即封装的javabean
    @MapKey("id")
    Map<Integer,Emp> getMapByNameLike(String name);
}
