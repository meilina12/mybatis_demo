package com.mybatis.test.dao;

import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.annotations.MapKey;

import java.util.List;
import java.util.Map;

public interface EmpMapperPlus {

    public Emp getById(int id);

    public Emp getEmployeeAndDept(int id);

    public Emp getEmployeeAndDept2(int id);

    public Emp getEmployeeAndDeptStep(int id);
}
