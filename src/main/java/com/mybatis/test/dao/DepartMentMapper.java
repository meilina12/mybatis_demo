package com.mybatis.test.dao;

import com.mybatis.test.pojo.DepartMent;

public interface DepartMentMapper {

    public DepartMent getById(int id);

    public DepartMent getByIdAndMultEmp(int id);
    public DepartMent getByIdDept(int id);
}
