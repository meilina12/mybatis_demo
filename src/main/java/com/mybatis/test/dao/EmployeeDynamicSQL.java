package com.mybatis.test.dao;

import com.mybatis.test.pojo.Emp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeDynamicSQL {

    public List<Emp> getEmpByConditionIf(Emp emp);
    public List<Emp> getEmpByConditionIf1(Emp emp);
    public List<Emp> getEmpByConditionTrim(Emp emp);
    public List<Emp> getEmpByConditionChoose(Emp emp);
    public int updateEmp(Emp emp);
    public int updateEmpSet(Emp emp);
    public int updateEmpTrim(Emp emp);

    public List<Emp> getEmpByidsForeach(List<Integer> ids);
    public List<Emp> getEmpByidsForeach1(@Param("ids") List<Integer> ids);
    public int addEmpsForeach1(@Param("emps") List<Emp> emps);
}
