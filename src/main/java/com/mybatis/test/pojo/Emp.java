package com.mybatis.test.pojo;

import org.apache.ibatis.type.Alias;

import java.io.Serializable;

//@Alias("emp1")  当起别名 不同包类名相同的时候  起别的名字
public class Emp implements Serializable {

    private Integer id;

    private String lastName;

    private String gender;

    private String email;

    private DepartMent departMent;

    public Emp(int i, String lastName) {
        this.id=i;
        this.lastName=lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DepartMent getDepartMent() {
        return departMent;
    }

    public void setDepartMent(DepartMent departMent) {
        this.departMent = departMent;
    }

    public Emp(){

    }

    public Emp(Integer id, String lastName, String gender, String email) {
        this.id = id;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
    }

    public Emp(String lastName, String gender, String email, DepartMent departMent) {
        this.id = id;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.departMent = departMent;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", departMent=" + departMent +
                '}';
    }
}
